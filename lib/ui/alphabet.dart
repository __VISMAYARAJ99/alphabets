import 'package:flutter/material.dart';


class AlphabetApp extends StatefulWidget {
  @override
  _AlphabetAppState createState() => _AlphabetAppState();
}

class _AlphabetAppState extends State<AlphabetApp> {
  int index = 0;

  List letters = [
    'A for APPLE',
    'B for BALL',
    'C for CAT',
    'D for DOG',
    'E for ELEPHANT',
    'F for FOX',
    'G for GOAT',
    'H for HEN',
    'I for ICE-CREAM',
    'J for JUG',
    'K for KITE',
    'L for LION',
    'M for MANGO',
    'N for NEST',
    'O for ORANGE',
    'P for PARROT',
    'Q for QUEEN',
    'R for ROSE',
    'S for SUN',
    'T for TREE',
    'U for UMBRELLA',
    'V for VAN',
    'W for WATCH',
    'X for X-MAS TREE',
    'Y for YAK',
    'Z for ZEBRA'
  ];

  List imageList = [
    'assets/images/apple.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
    'assets/images/ball.png',
  ];
  int index1 = 0;
  List images = [
    Image.network('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVFRgSEhIYGBIYGBgYGBgZGBgYGBEYGBgZGRgZGBgcIS4lHB4rHxgYJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHxISHjQkJCE0NDQ0NDQ0NDQxNDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0Mf/AABEIAKgBLAMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAACAAEDBAUGB//EADwQAAIBAgQEAwYFAgYBBQAAAAECAAMRBBIhMQVBUWEicYEGExQykaFCscHR8FJiFSNTcoLh8RYzQ6LS/8QAGQEBAQEBAQEAAAAAAAAAAAAAAAECAwQF/8QAJBEBAQACAgIDAAIDAQAAAAAAAAECERIhA0ETMVEiYXGBoRT/2gAMAwEAAhEDEQA/AJRHEYRxOTQhCEEQhAIQhBEKUIQhBEIQCEeDHgPHjR4DxQkA1JOigsfTkPM2HrITn+awI6DT6Gc8vLjjlquuPiyym4ligI4IuD+47EQpuWWbjnZZdU8UaKVDxRooCjhDa9tI7uF8K2NTmTqqdrc2+wgYfEMH93UYsrhrX/CygsLdBpa3ecb5sZlxjtPDlceRRRRp2cSjGKMYDGMY5gmAjGMeCZAJgmEYBMBjBMIwCYANImkjSNpBBUkckqSOBqCEIAMIGUEIQgAwgYBiEDABj3lBgxxABhAwCBjiADCBgFHg3igGw8D/AO0D6ustlfDtKi3IYDcqbemv6Qmxi5V18JGnn0nk80/nu/j3eDvx6n6z8crIfeJ6jkw6Szh66uoZTofqOx7yviamYGwNpz+Gxj0Ga6E0yb8tuovz9Zrx5ac/Nhvv2628V5Qw3FVcBUWzN8uY2N+lunftIDxVixREUsp/q+cL89u4InflHmuNjWvIMVisllU/5jDw/wBi7ZvPkPUytUx2UBsuYNsAw0Hc9fKZCGo1VqlTtYaaC2w7AC05557mo64Yd7rew4ygC+sFXvWpjTRr/wD1I3mc2PAFrNfyhYCveqo7M2v+02nDjbY9vKTG/wCGzFGjGe18w5gxExrwEYxMRjEyBGCYiYJMBEwDHJgmAxMAmETAJgC0jYwmMjeQQvAhvI4GmDCBkQMIGUSgxwZGDDBgGDHBkYMe8CS8IGRgxwYEl494AMe8oK8e8G8cGBLT33t/3Mt66I/uvxba3+a2oWaKHWVsXhyKzsfEjBWa+wa2+s8/mk6r0eDKyWfpNUsCxWwHrcdjMHidcsSdgOXMi/6H85tvVLagXUDqdx59jMHFOXzspAZPERs1gBfSZwnbWeXTHSmzV1UN8gzc+ewHpzg4XMagUEg3IJv2e9+nL6y1imvTp4pBqGym3S4Fu+4P1lYC1YOL5dWtrrmB07+In6Tu4BwbMuZC9yrm29v54ptYXFFVsdb3GbcjmdJjIMmHFRl8TufQlifyMu0ahV8l9x4vpbT7zOU23jdNLGVWABXUeV9fOTezzF3LlQMgN+5NgP1lZHC6MbDz0mp7PBQKir83hb/idJjGdx0yyvGtWNeKDed3lPeMTETBJhTkwTGJjEwETBJiJgkwETBJiJgkwETIyY5MAmEJjInMNjInMio3MjvCYyO8DRBhAyMA9IYU9JdVBgwgZFYx80aE1495CGjhoVNeODIxfpDCHpLqp0IGFeR5T0jxoSAxwZEIQMgu4CnmdR3ufIayvxWoz1TTp2CjxOx59BLmBfIrOb/KZyPGMVUIy0yVNRtWF7lQPyuZw8n8rp2w6m21Wx9Gklna7aCy2JJOgsBty3nLVMfTSoCyOpbNr4TmXUNdRoR+0dsKgpNTNwzWOe1yGU6ZrcrynXV2ys1EllBCuhDKfLXr16xjIZW+o2lwKjDAU2zUySfIEn7gE/QSjk8emh0Gwvptp6faL2bxrK70anhVgcq/0kb/AKGTmwq5+QO32iblsrV7ksHiMMgoo9Q5VR8wFvm3y6cztp1mVSxqF2dKbeHRiWAY+nL6yXjldnrJTpWYItrHQXI+x0kC4Jxf3jIivbMEJZ3A5AcprHWu2LvfUaTYym4XLcEgHK3zC/rrL/AnyVsrncFAex2/SZFfD+9F1p5VQWQ31Fth3knCazFkz/MCB52Mnrpt2LixIg3hVLk3tvIzO3bgRMEmImNaAiYJMcrBMgRMEmCTALQCJgExiYJMByYJMYtAJgOxkbGImA0CNzBvEwjWgdQMOIa4aaC4eTJh+0+jMcXk3WScL2gNgh0nQDDQGw4kuONOWTB+EHSEuE7TY90OkJaMnDFeWTMTCyYYXtNNaQhimJZInbIOF7QThO02fdxCjGsV7YvwvaEuFm0KMZ1sCdPoJjKRZtyPH6tl92hIO+n82mImHLI1WtWAFsotpkH9t+c1PaN2U3a+XtYaHqZzlCiFbW1j8pbxDtYHS8+VbvdfRk1qJfeB1yIzuo2LU2Nr7gOhBt6GQphnRroSvWwIzf8AEzoMGhFlqVE6gAa+uWW+KYIuqMrEWN7Cm/j12JmZl6auLkqVFxUNTLZ3OVb7gHQntKr4ggnc69DrOvThvvUsc6upurgMljyIuST6zBfhtTN8Nc+93z2Fsmb5r9cunW/1m8ctpcZPbPw1Ms4qKPELg+Ww+mssLhH1YAsx52uRfrb1m63ChRUBSS5PiZlZs1972jcPw+VTd2N+RRlAvyu17xyOLCLrcU3zZ9CrFgNemVdBCwzulVQV8OfMOe+4mjjsGW1awA+UhRe4210lTh+d6y5mB16FTp2Ill3Gcpp36UAVB7Rmww6QKGNA0bSTHFidZ5rHK+OVD8OsXuBJveqYLuJf/RPxPh/tC2HEE4W8JqoHOJMQOs1j5sb9xm+HKfVV3wPaR/AzXo1QZOKIM9Exwvccbcp0584SAcHOl+EHSRvQEvDFOWTnfgoJwom61CQvhI4xeVYbUR0gGh2m+uChHBDpJwi8nOfDHpG+EM6L4KL4SanjjFyrfShEaUiXFxvibzl8rrwShDHGHJhUqsv0SDL8lOEUFwvaEcPNdaYiNESc6vGMj3EY4eaj0ZA1AyfJTjFVaMlFCGqESUR8lOMVGw8q46nlQkzUzSlxUXpsJnLyXVXHGbctjAlVLEG1um/lfeci6Ol1NMCmNS5I8A5FnO3kLTWpcQyOyVPlU79SdlH3l3H4ZKiqKmi7qi3JJ6m2v6+U8U/t63OYNwpz03LoDq7FsinoB8ztqNPKdNgsajjIzG3nY3G97HuNOV9ddBy/FaLggIQEW92A0przCr15ZtyTYWB1ho4wKQtrAHKdb5QupX72PUs3aa1L2m77dwMIyjwBSD6fXczIPCagre8z8rZcuhH5ylguLVgLq1x0Ox1/7l1/aCpzprfrrJ1F7q+9Alf8wBQOd73/AGmTj+JIgyLtrrvr5b/z0lTFY+o9szG3Mch/LTJxmLXUW8R+p1+x5/8AmJjurvUSvjWGpqEqdh8wb66HyI9IVDFqtnfwclA+XzZNh6ShSXJd3Ph5qdm5gj+aR6dM1XB/B5bDoZuSOdu3TYzFtlW17EXv+E/TaU14syHK0lSqGTICuVRYXvYzD4rWa4p6ZRta0km+ktdAOLg7GX8Pjg3OeejEMvOaeD4hYXvGXj/FmTrcVV00MoUsWc1rzOTigO5lb4q7grJMS5OxwbMTN7CMw3mHwcMwBIm41cAWMuHlyxTLCZNJHUwKlMTPZiBcGSYfFZhqZ6cfPMnG+KxYFKL3EZK46ydXvOszc7iBaEZqMsgQHM3yTiqulpSeprLmJqACc/XxOu853y6Jht2aYFY/wA6TPTHuN1lhOKdQZ5uUenilbCWgKGWH/iKxDFoZeRxSJiiN5IOIiQEqZGaCmORxXhjVPOOMQDzmccIvWCcLbZjHKmmqKgkgsZie5fk0s0g4iZJpp+6EqY6h4D5QRimG4keJ4gCpEbhpxfE8Ej2cDxJc5b2BYfr35ctdRyGKxtem5fN/mNptoiDQ2HK527DvOnx+cMalNr07tnXrboesq+9pVxYgCpzHNegv9Jwt06ybc4eLMiA1BdmOnK2umnbf1WUnxC2zU9NBcdbgX/P7Td4lwvMwtYjRQegUfw+sxsXwZwbgHTp9BNY3GlmQKONXkWVj02J5maC4s20IP2+swnwFQcuRtpaSUaFYDKdR32vNWRmWtHFVnYAJoN5nkomrG7yo9OrmIVWF+XTyl7h/Cr+OodByMupIbtqfA4dqoHIC/hJJH83mjiaiohpU7e8b7dZTr40/+3SFrWF+Q0keHUJdma79T36Xmdb7pv1F7CuqLYgNbU9vO8zq7DMTa15LhcRqzML30tzjYrDkciAdri03PtKp16FxdZSJKy7TrZTlMsPh1aXbOmUjMTpN3g1A5gWkFGiqy7hKhDXEzlemsce3b4XGIiW5yjieJX0G8zA5O8idiDPLXpmMaa8SfblJqfEbDWZDVdJGKt5rFjKNROOZXsTpOg4ZxZX2M8y4i5BuJJgMcyEMGInpx3O3nur09gOJFt5n4niKruwnO4fiTVU0aNh+FNUN2Yx8vpeDRxfEwV0M5mvXJYm86ih7OpzJI84f+C0hpkEz3e6ak+m9Sok/MLQmw4mo1OVnw68zaVdstcNrvLSYYdRD+CBNw5+sVXCH8NSNQ2TYXoZE+DcfK8ifCVfw1BEmHxH+ov0k/wBB2pVh0MgrNXGyg+suZ6675THbiTL89M+msaFPD4uoPnpmX04ovMEQafFaLabHodJJUoI4upEkv4uk64xGG4lTFFGU2te0grYFraCZtXButzYiW1JGJVpMabe7N8jEW/t5zn8qObklH3J5kX10P0mn8cULqouC2Wx/Fy0EwuKYizeEC41JHbl9dPSZ1druLje/Rt8629QBz7wk4sCchBU8gdNtpjjjTJYm2g1767CF/iyOfGo0IA7aXP2k4X3GuU9VsriVJAHc9bXFh+khZmvYAFf/ACQf50mZTRDqjEC40v30lzCMyMQTfv2vb66Sa0su06G7bC35aXmbxDEjUA6ba7A9JadjdgomJimVtGaw6fvNYztnLLoHxV/CgsvO/OJ65+VdfTb6xi1MCxb/ALhJmcf5ahV/qO86sbFRAQ3c68h0m98aalG7Joul5zdai42ux68pZ4U1Riafu2a40ABvJZvtJfQlUE2Ilp6ZUQXolTYgqwOoO4l11uusWtSM2g9zaatCnbUynRo2N5aNSc8q6YxeDRqjSBHid5y4uvLQGeAz22kTZmNlBJ7An8pPT4XiWGlB/UW/OdJNOeWSniUzSlidFtNluBYr/Rb6r+8p4nhOIHzUW9NfynSVyvZuC8QKCdNgfaQCy23mTwr2dO9Q5R02Mv4nhuH1VX8YHUG3pOeVxt6aksnbs8PxAMosReBUVib3M5jgOAdDdn8p0uYyc14uzDwWAPKQrHDTttz0dsKp5WkR4eORN5Nmjh+8ChW4Wx+VzKj8JrjaoZuZj/CI4bv+UvQ5xuF4j/Ukb4XEr+JT5gzpKlVxsl/K0D3lxcofprM2LK5h0e3+ZQDd11+xkCaH/KqMjf0Pf9Z1fur6gFT3EzsZw923RHX6GY0u1bDcdKEJXW393I+s067o6XBBuJz+Lw1RBb3TunNSCSP9rc5jpjWpHwsTTPI3DIeljG7PtdS/Stj8NTRHeo1jnbLa3hnL1OHe8U+7DFBrcc97AHa/7zo6nClr1BUrG9FBcLfQsTfWWcNVRwahA9whsiDZiPxG3eOWkuPpx1P2fcpmCkqCAABfcA78+UqVOGqo8QI3JNjyFj+c9HGNeo/u6ahQBmYnkSPCAOvnsLSKuiuDTrqrX3t4QO2a/i9Ink77OPXTz1aIU3BsBtJauNGp/FqPO+/7y3xXg70nJQF6Z1BGtvQbTFqsBoef2nTUrO9LmFqENcNfWZeKQNUa3M3t0l/A4V6jWpqSR9PrOi4X7Mqj+8rkE7hAdz0JjlMTVrD4XwXOQdAORb+eU2KlJKJCutl/CxsVfXSxj8SxWJzXC5KY0AU5bDtyMehjg65KmobQgggH/wDLDtMW29rNRF7pKhCocr9fwt6jY9oCq1BszVfFY2AVgQega1j5gyDG8MdLVKRzW5X8RA19fz0ltyuIpG3zLfQ/MpG4POWf8KCpi0qC+YlupJJkLA8jMig5V8p62nRYOmW0VczdBNWaSZKtKmx5S1huFu5sgJ/L6zqOGezjtZqhAHQazpKOHp0xYAafzYTFalrlsB7JOwHvHt2Gpm3hvZPDpqyFz/dr/wBTTfH20Ubf2n8rSs+PRjZg4PUKbH66SdLdpPdIgtTpoo7LIfiw3hvY+UiOJpA3LsP+LA+e9vtJKWKoP+MX6kWOv2kGfiOIVEbIw069RKOJxjjxbqd9Np1FOgh1Lo68r8vKWsiWsEU9rARIfTjDWZyFte+1pcwHsnh0f3tS7VDrqdB6TpFpLfWmottaxirAc1vGOOkt2JcPTsLINBpK1Sgt/kERqAnwNftzFoYJ5jWW6NVdVzzH3hmUqdeTB5pFoecXrK+bvHUnrAsm3X6QS/S8ARXlEyP1MWcdz9TI7QgsBzU7QWrN6R3NtpUquZdmlg1O/wB5DiKauLOqsOjAH84C1OQEMJ1lmqMvGcGpuhRVZATc5G3t53+0x8Twl0VKdOz01a5v4La3FxzA/adaRaV6ljpJcYbYj0VRMvNmLOf6u15jYrF00e6rma2t9beXSdLisGHFgbTEx/s27HMjDTqN/pONxu3SWaY1P2hfMVCgdtJm4jC0sVVBqeBzvl0DGPxXAPSbO9MjkSNQZnYJKr1Fc03FNbkMFOv2m8cdfTOVjrsNglwyuiWWmACSdWczA4liWZc5JGvh5GSUfiXfLlLICbFjYkcriNieD4pnDtTBQcgecnG77JlNaJMUxQipsBseciwGRlzqNQdTe/paaX/pqvUQhmCX9TFhfY+pTXItQWvqbcomPSXKbU8ZiAgDcyRb9/pJqYwxIdnam5BzWUMpB/q0H5zpMNwCiABUXOR/VrCxnAqLgALlt00mtdJvbjF4AC2aniKbISD4iUYehuPvOv4Vh6NFQDUTuQwJb6cpn4n2YI1Txdtj9pRPDGXRg49TFtpMY6vEe0dNRlpgt32Ew8Rx2q17PvyAtaRYfAAfgJ+pj0OFvmIFM25GRfpNQ4lUHiZyfXeXKHHHO+hkD8FqnWwA5C8hOBYWBWCujw3F1YWa0lekG1Qr5MAZytfDutsinTeavDar2BMkJlpoGiy//Cl+w3j0c99aK29T+striISYi50lvSy7S0Sw/CAPM/vJGe3eMhv81oDuNhLpNo6iKTmGjdR+vWT5GOqkWkJEHP3mbDaqQ195Kjm28UUsQSVzJhiIopoOuIkwrmKKUP78yRa8eKShPWkLG8UUA1IXUyrW4hrZRFFAZapO5kbnpFFIQKsestI+kUUor4kI3zAGKiqbBRFFKBrYRL5raxBFIsIopUJUtDZOcUUIpVmF5Hn6CKKZaTUyYZcL8wv5xRSUhzilAuFElw+KvyAiiiFPWqyJUDbiKKZqwT4YW1Egp0V2AiiifaLJpgCZ1a6aiKKbv0zElDFBtzaThlvcGKKZjSTOeRjFY8UtSP/Z'),
    Image.network('data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUWFRgVFhUYGBgYGBkYGRkYGBgYGBoaGBgaGRgYGBgcIS4lHCErHxgZJjgmKy8xNTU1GiQ7QDs0Py40NTEBDAwMEA8QHxISHzQrJCw2NDE2NDY0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NP/AABEIAJ8BPQMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAACAwEEBQAGB//EADYQAAEDAgUCBAUEAQQDAQAAAAEAAhEDIQQSMUFRYXEFIoGREzKhsfAGwdHh8RRCUmIWI3IV/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAECAwQF/8QAKREAAgICAgEEAgEFAQAAAAAAAAECEQMhEjFBBBMiUWFxFEKBodHhMv/aAAwDAQACEQMRAD8A12hNaFwCY0LzCDmhGGrgFKAOhTCiUSB0cEYS5RApBQcLlwXIsKOUgKEbUwoiF0IpQykI6FICkKZQBELkQKgpioiFICiU2hQc/wCUSmlY6AAUwr1Lwx51hvdWB4Yzd5+ipY5DUWZELoWq7wxv+1/uFTxOEey5EjkaJShJA4tFQhdClCSoEEuQAqZTQElLcjJS3FA2cuAQyiBSomyYUFc4oMydD5EOXKCUMpMOQRQlQXIS5A+RCJBKnMhIObFhyY1yrsKLMiydljOplVg9Ma9FjVjQmJLXKS9PQbDlE1yVmQ50rDZYzIg5V2uRAosNjwVOZIzKQUWA0vUByUXIcyLoVljMgNRLzIHlS2KywyoizKswq/gWAmXCQqjscbbojDUy90e61Ri2MtAtt/SpYjxWnSYYaB23PUrFp+Ltq5pBDmuAEOAmb7awAto1HS7OqGNpWz0Nfxi9j7KtiPHGsGYm24XmcTjGgyXQ3fYR+5Xm/FfEsTiXZKVNzaIzDK1rWucdA97njTcAequNvyU6R9QdjwQHsMgiQQrtDESPNobH1Xjf0ayrTphtcgkHQCw9fX6L1VSvRDTLhEEm4tAkpeRtaKOKZkcW8KuXqjV8fwjgA2sBlES98k3tJJk900Pm4Mg3BGhB3CwyqUX0cclTLAcpzJLXLi9QmSOc5Ke9QXpTyhibDzojUVeVxKSkxDy9DnScy4lHJjsdnUFyW1yhzkOQWSXLgUqUWZKLAlzkOZC5yHMqWwBa5OCQ1qewJ8TsjBE5VwTIQEJUPiiZXNQOKljkUJxRYAQFdnSn1EpGcmkOYjVUVkbaqSkZuSGprFVc9G2oqTFasY8pa5z1LSlJiYxoQuCmUBcnaoQbQmvxGRhd7JIcsj9Q1nHIxp+b97Jx7NcKuRk+O+KOyN3LpIHPBTcAXspgSM7xLjuJ0A9FkeMQajRsCGjsBAHt91q0XgbzYLZKkdknui7Q8MDruku9StbDYJjAHVKhDRowb91m4bHBoLok87BYmO/UzM5Y51xY8SdB3VRvwZt/Z6OvjQXF8hrdGjkDhZfjeLbWY5he5tjGU/fnrKrjxIFphw7gj83+yw/E/EGjVwmNJkzPA9B6Kox3YSlqjy1Sg4O3deNeTovuXh+FDcLhsoiKLGOAOaCxoGu/f7L5Xh/C61Wma9INDW2eH+UkTyLRK+leBYzNhmsJBLCJI2PQp55JxoxcW4tltC5ESlkrgk6OYmVxC5rSiyqoysrixa6FLgoASf2KiCFC5xQFyfIQaB5UtuheEPY6AKgvQ5kMJVQgiglE5DCFZbSXRayQpTKpQC6pS+Nne4/KiWvUpjKaEiEKSZMotA5UBarTGyh+HdEpJIXFsSWoHU5VpzE2jSWDk2y/ZjxMipSMqWUncLcGGCL4IUymokQ9Jy7McUnJ1OiStMsCGwS910afw1yKLsOVNOiVeBBUtIR7jS2H8SLeiqKRUnDlXDCgPAUc5SZqsEFGij8Ary/i9aa8bAgegufsva/EavOfqDwdz3Z2azPtf+AunHOnszjhUb4nj/Ez5wf+8+4K0qD2xbhZviLDnLCL8dguwktMEW/NV2LcSJdms3EyC2F4bHYB1N5LxILi6eZ683Xsns3CCpleMrh6FOEuLIlGzy2Hp04Ba8G5zMccrg0CRr8x19wr7fB2uMi++nPPuoxvgrSC5liASANLbQVTwb69Ocod8uWCSQBIMBs2u0LpjJS2ibrTPf4LAOfhfgsjNnBcHHLLbkSd/wClq+FeE/BYW5i6YJ6G9h0iPqvN/pbxPE1KjKfw2NEjM8yAALmBHHVe4K4/U6kqNLi4tEMoJb8OrTHoyFwzm0XDBGWiuygYQ1GK38QQqlaoiL8oqeGK+Ni/hoxQKKlUCsPrCE5ZG9ExwRjspPoBJdQVh75Kl7wFCyNaLfpoy2hTKNkjEMVn4iU8SjHKSlbDJhi40iqylKJtFWmMgXSnVBNlopuzN+nioqxNXDkJWVaL6ghUXPuVXuuif4qb0w/iSSpoOvdUhUuic8qJcukdkeL2zT+IkvJSBWshNYpqMrM/h9lqlVhMNVZ1Ope6uOgixSbV0Li1bXQ0vU065CrZ+qbTIKbUemSlPtF9mIBCH4ypkkIGuWft2zf3eK2W3VZKAvVFrzKtsZOq04pIy91ylofTqjlPc8LMrHKkCuZUPHaux+/GNo22kKvVeFSZWcU+lRJ3VNqPY+XKNoFxKu4AZzfQXKAYU8pnyMPJMey1xqMpUZNyijz3i/hjHYh9UCAWgNGw5PtZYL8HBMbL1eIMgrGqMubD1XW4qzK7KbHC1kT2MdpEozRjZK+HE2TcbEJZTBJi8EfVbGA8Fpv+aQdbKphaJJzLVwFcXA/PdKKG2eg8I8FosYS2ZO5S6ggkcFafg4Dmtg90vxvDhpBaAJGyjLH42EVcqKtMgKX1As97igp1DeVySx+TohkUXVFt9QIS0Km65mU5jgN1knKJvLHjkr8nVFDDyoc8BA+SPKlKV/shY0lvo6tVDVUfWJKjEUTuup0pC0jHzIyzOtQCOIT24qFWNKCnvw/llJpS6LxqTpSR2IxUhLwzZEoKDQbFPDIWMmkqXZ1KCffQGJslHopxzCWwEqiYELaD0Y5cK/pYoOEppqBIxMCSNlVdWOy14t0zllKm4vRea4lOZUA1WZQxJESd1eqPYYlS5Sb4tAsKpSUrf0G94XNrRuoyyMrRKq1qDhqmoq/yVmk4xp/4JxtV0eUocBWeNSpptIbypYxw2Tljjewx+qmoUlpFxmLO6P4wVHMoYSURgl0YZcsp02aTK7QiOLWM95tCt0pOqTg7scZRcd6ZfY4vOigloMIcPXLdAkvDi4kqlF3RnLiladss5wFZw+KA3WaxhKn4FraqJQjLs3wzcXSNulULjZF4w0tDW7xJ9UvwChNzo25/ayHxCoXvcesLo9PiUU2PLkcqMeo4wqdQX6rSewbqnWp3st6M7EinOh3P2+5VatZ0f40t9fsmPeQU+kA7X+1SYULrAspkjUGB+eiPAEyIJBmDwfwhWq1AOYRtI+/9qvScGGwmP7/lDBHtvCGBl/p3Oy08awPYZAsJErzvhLySCV6RglhHIKdWqE3Ts8i+q1shVmYkEwnVcKC6SUnE0solq8eU6lVnoKK49f3LLaTYlVqjOCksr2um0aciZV86Vsza5PjEgxuVOHxYbZLr0uqS1l1UIKS5GWTNKDUaLdfEgrg8RZV304UUjBunkx/GkOHqLlUkDUc6eimniZbCtGs2CIVDJ5rcrLFFq9HRkypUkNDHNvsmYirlhHWboJsl4zDWBBTjDktouUuMeViW1S4woYMsg3U0GQZJUVWyZG6tR46SM/ejJKiqWi67K2CgLr27QiZDniTA0JWz+K/B5ik5S6th06TSJRvwwdvojxTWMb5XTokDFCdP8IhOMlaCSnjdeQ8NVcx0LTrV2lkkXCzhWBP2VnEtzW4hJ403ZcMrqpbBZVBtC4uElIY8TBtCNzryL2S40yXlbVDGUGkaqGU2gxsopnUaSJTfhnKCR6pOLfTBSj5RxpMNgpLIQimRHVTVN4laKorbIrl0ixSZIupeGk2QaiBqkMHulNt1Q4OCux5gWhQ2AZlNayRdVH0i24Olz23VOIlJtm5QrBjNbm/CpUnzJ5KRj6mUNb0CjDvt2kroh9Gr6IxLtlUcSjrVW7ugzbqkveNZn7J9gVcQXgzAjiJQ4et06KMTiJaYNxv91Sp1+Lnf+UIZvUcQA2TvO06aT7quyowkXm40/lV6j8rLf7pngXVfBVpiNkNjR7PA1f6XosA8kGeLLwmArkWMzNpXrvCHmQDvZNPZLR57GHK6J1XU6zLNNxutr/x05yTDhtf+Uup+lnXLXNB4/tefLBKb6r8nZHNGCq7T8GVi6DHDMzbVViwltirzcM6mHB4IPVRRw+ZgI3Wf/mLjLbQ44+T5R1ZVb5QLKc82hWn4R7mQ0aKr/o6omy6MaTimkcmRzjJp7IdTdNhKTWY8XIVhmdpE6plTExr5uiJcl0hRUZL5PZRYLqS4EEbo31BY5YkIDSGU8oSb2w5JaQDAdJRl1olRQwriJ2GpVHGYoj5Gkk2VJopKU1xb0WwJtKdTAhZTGPaZcb2twrT6/VC+yZYlB0mJqYeN7kyEvJFiOxGhUPqO1Oummt017iRA8uhk97p/s59X8RrKLWmIkH91Te5weGtaIdMb6f4Ta78nmJls+Ubk6QhZ5vKCGZRJ1JIkCAdtUKKa0b48rg7fZZpsAImxI+q1fiB7Jm4uesbLKqed4kaXPbQyEl1QgOax0E6RvtI+vssZQvp00dEMjlprT80XcaWQxwES4AnqeULQCSNDG+ip1nve3JN5APXg6cgKRVsAbEG/U/gVxVaZx5ZKM2114ofh2lts07np2KvPxAOUAWCoh0WN7TbW5uhNQmALgiZPsjik7Q5ZHNbNLI95kDSw+yVVwz2GXNMH19zsk4esWtPmsRIEk36cWTWYt1tp+YdOFlKUnKklRtDHBRttp/QLabzo0nsjZQfrBBjcfZOFcFrWMGWbme5t+cIGYvLmZmtP31ji4RHJJN2hPDCrsN1heRaOiZSM5WGPMRfWwufsqDca4mC2WTqbev8AhXPDntcXOEgNmxMmSPpqtYzUpJUN+mcY8k00V/Fakvnqpwo8pJ7D7pWNuVLKkM1i66SRL3w75fWEnEMfIO26tU3SVbdSBaeycRs8z4gzKMzbT+FJwzNTF9v7TvFrZQbHN7jcJLKlpBVCssPLi0N0A9wkYJpzuB2P5CN9fyE7gFUfC6xe55HRo9rlNIVno8GSXzJgey934MdJB5Bj915TwXCgxwI/AvZYJwAhp9NEumV2jQdVuQjY9Y+PrkPHVs+xhNw2IneypPZLWi54nhW1GERJAkd14QVi3QmxuF79lVeE/UJLKzmtYCC4EWIiRJv2n2WWaMWuTRMXJPToa7xR+XKNxHuhPxDEvMn/AGg3WRWqQQ6DEzMTpqAFbdXcXxki29o3k8rinKcWuPR1RUZqpPZFfEus2bmR17qX4im0hujhu7dU8S25czWY8x0NiSf4QvznyvIJgTlAgTytOXJJpmLjwvl+i83FsLg07x5vVFii1r3BrgYsd+0rOp0Ghzg0WiSNgBYQFLwJcW2Np2m9/a6qn5M5SjVRRebUeGwPyUl1RrTfW5HoLpbHmCOp521/dJqvaTMSPLqZuBePzZFCTflllwBmdbFXaWHoQs51eBPM9+54CClWBHmjp+eylwb6dFQyJdqzPY8GPNOWdOdPXX7qySZgkydDa94PQWKpMpwcwsYPE/LBnnf6J1J7mgnW8i1thoOwstU9HOGz52wM0EwCdxMWjufZKqYgudOWCJDvfn0RukGbDSJMbTE73QVKnTqbbTI+6Gxro0cLiA1vyy4A6gHi2k7TbhJxOYOnLAPlBIInex7m6VLTH+0xFhcx+9902tUMNjzBsxm3AgBx48x9oUKK2/s1eXSrVE0hckuAiDroSbbcomQfmAsSO+9o21Vem5oG17nQ36EqxTZIgETbWYna/dMxaOrYepUH/qFxIjMJgkDMAYkC6PD4V/y+VsARJkTJ0LZtI+vRJY/I50EkgEt1aBpcwd4TsOHa6gNueOD1vfZRJyfVbNYLHrldiHPc0kRoRPTYglE/FWyzdsEibiND+6ZUYT5r3gRI2gAnr9L9kbGs+YdoOwJv9R+Silohtp9v/hRqYp7HCQA3O4OBs6/BNvVSMQGtLQNRYx7Zjzr7rUpUQ5xDhnY4ZexGhHaJ5VWphhlsB8w3GwuI4+0pyWgl8o0Q7EZWjNpnAF7iTYesQrOFqsbLWuzS2ZH7jn+FRxrZgu8zsxcADpfy+3VJp4locLBsyMwBIPe972VRStMqM5L4+C1icUA6L3068x/CMvloI+6pYwh4yFpvcEcQCHNPNwm4HPkh+uoPIjWNltejVS2XMONNR+61aMEeizGkW0v+2ytYatcfnqpUqZbpo8/+r25QwjY/Q6rzdPFwYO5H8L0/6zYYAAnccxp+d14qrTd5AB5nzbqHOBHsFvaZkalXF+Uj091e/TtHKHTqTKq4HAyx73wTkD8sGYLm5j2u6BaYVjBVIJIcC3k2mdJGgO2uqExLZ73whgyCNTPqt/DDQ+681g3tYxvJub9JF/qtPAeIgG4WUpbNorRq+I0s+QbXJ7I6GHRQHAESRtG/Qrq1R+WQBbYa+nK1TVWRu6AfVDXALD/UILnmBMtFugBMiFYxWPaLu0gkzqCNUH+poPaHukOjY6y2Lg9FjN2qG4t9GCyjI8zocDm1B/6676jRc2o/M0nS4M/Y9CFYxNRkgNe3pOtgBtrY78JJaQDA81/LJMxpB3N7aeq52k9Ml3GV1QqpTaHvcwEXu0kO2gi6L4LANXSQCI0MH8HsnitTHmeMwI5LYLiT5iNhlneOypYcOeXQYImRZxAkQ4yIOm3/ABCMceLo0yOco8tV+/Ix1KSS2RLWiZA0Hmm35CVWw7RmgOECxIh1jrB6mffVWGVAfMIad2zY9YO4/fTVV31S5xEjyzljjKLCf/oK31ZzL8opVHvbJY3MDmOW8mG3vtoEXwrExaxHIIBJBn3twml8BrheRAm1gWgSOsn0UOr+QSQMxeXFugAc9hI5t7+qdFudxqv9kFlpFrxcdduRcKcNQB802dFidDede49kj/8AQlriNxlbxq08X0PuEFbEsFiYG2nAnXqj8GTKbrFp7n03n6Ky18GRMGLeiNlFuk9RI5FwodQNhbW3snYU30QcTLQDGvv+FGyg2MxHExumtpBoMgEgCeJjUImGLRYifopbQlGXginREkmbmR3Ogj1+iP4MEg30PsIH0CdQdeDp+SlPJLssnT3Sf5B2G1hdpB2E7dPSUFR7W2y3mx2MXgnfRHSpH5iYmYA4nbhF8EEX0Jj16bi0prXYWwG1s9mgk6O1sY0O2gTKzXNbOW28EEWnUJLMOabjecz5ncRwrBm4DzbUxEi4NuxUO7OmPtcabdlcVDJ3Bje5kbcf0idWlugsRpawue8qfhhz8snkfsq+Ia4nI05QbzvxHurOdh0qzjFo1kn6FXaTt9ybERpMz1uUvB0oAl06zIvKlxDSDFpSaphZz2gmMotP9+qCrRZkgDrH/YGQR7fRMcbAjkz6rnFoBJJJ276JbbGuhDqbXeaLRHQ20H0TQwQRNgBlBkwN/TRNzAsj6KC2wJ1tHXmU9ibsXgpBIIuCe12xdOrPh9haTOxAgWEa7qQNCDJJAkiET6BvvYjXc/0h2hptIXjcMyo5jiYykwDoQWkG/f7lJxGApSxwYMzHOc2BoXWHrY+6c2XEN3kzxohpElwn06nqjmxOTYFPCsl7sh87IdO8kx2FyEFXBsJaBDQWtcbWDmzJPc/dWqlUG2/OwjhJpxJGp3J7J83dA9B/CsHGSBHvEQOis03ttwHNntMx+cKtmjyluYa6xBSWYi8GdbDZSpfZXOXSPSnxMAODXZRJAI4AN49Psubi2eWZnO+XB0kNbInXqF54VjERrH3/AD2Rh0NkC4kGSYIOoI/hVzsrnKmy/wCNUcxMOa6bG8AgwQ4bgxxcrKqYZ+XyaNiZE7/LB2ib6qzUrHKCDqZEjcSPaZTA4AF+xMQN7eZNuw5yZmswrc4eDcEi/fWPzVPqUiSRm1mQdo4i8CEyo2YP/ITxPdDUpeZp41AnU2vzupqyba7Iexz2xYwCCbGfMSZzXdr3ul4ZmSZ12OkgaCTsmPrBrjE7220B+6j4giTqRBtq38hPoOTqvBL8K7RsNzNABNwTBgunS0fRVDgCMxBBl14kFrR5SY4zHXsr2KeNdAPzTsELYuNDEwJ25Kf4E5Oir/pCXSYDOROYSWgRrIIBk7SFerU2E5XMBJhtwAMosRA00sNoS6dUluaB5Rl03Nx+6U6pN+/cWGiUo2qsqM68CcJhGsiwJEkGMu8/KLWtqlVaJJLgWtknmbHS0mBNp5WgfmgATln6X+6RVyzprf8Ab9k9olSP/9k=')

  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              onPressed: imageIncrement,
              icon: Icon(Icons.list, color: Colors.black))
        ],

        title: Text(
          'ALPHABETS',
          style: TextStyle(
              fontSize: 40.5,
              color: Colors.black,
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.bold,
              letterSpacing: 2.5,
              shadows: [Shadow(color: Colors.blue.shade700, blurRadius: 24.6)]),
        ),
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
            side: BorderSide(style: BorderStyle.solid, color: Colors.black),
            borderRadius: BorderRadius.all(Radius.elliptical(20.5, 20.5))),
        // centerTitle: true,
        // shadowColor: Colors.blue,
      ),
      floatingActionButton: FloatingActionButton.extended(
        splashColor: Colors.purpleAccent,
        onPressed: alphaIncrement,
        label: Text('click me'),
        icon: Icon(
          Icons.emoji_emotions,
          color: Colors.yellowAccent,
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.elliptical(12, 10))),
      ),
      floatingActionButtonLocation:
      FloatingActionButtonLocation.miniCenterFloat,
      body: Container(
        color: Colors.transparent,
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Positioned(
              child: getImage(),
              top: 10,
            ),

            // getLetters(),
            // getImage(),
            getLetters(),
            // getImage(),
          ],
        ),
      ),
    );
  }

  getLetters() {
    return Center(
      child: Container(
        height: 300,
        width: 300,
        margin: EdgeInsets.all(20),
        decoration: BoxDecoration(
            color: Colors.transparent,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(5.4)),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              '${letters[index]}',
              style: TextStyle(
                  fontSize: 30.9, fontWeight: FontWeight.w900, wordSpacing: 05),
            ),
          ],
        ),
      ),
    );
  }

//letter[index % letters.length]
  void alphaIncrement() {
    setState(() {
      if (index < letters.length - 1)
        index++;
      else
        index = 0;

    });
  }
 void imageIncrement()
 {
   setState(() {
     index1++;

   });
 }

  getImage() {
    // return Center(
    return Container(
      margin: EdgeInsets.all(20),
      height: 400,
      width: 400,
      decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('${imageList[index]}'),
            fit: BoxFit.cover,
          )),
    );
  }

  getPictures() {
    print('hi');
    return Container(
      child: Column(children: [Text('${images[index]}'),],),
    );
  }
}
