import 'package:flutter/material.dart';

class Alphabets extends StatefulWidget {
  @override
  _AlphabetsState createState() => _AlphabetsState();
}

class _AlphabetsState extends State<Alphabets> {
  int index = 0;
  List letters = [
    ' A for APPLE',
    'B for BALL',
    'C for CAT',
    'D for DOG',
    'E for ELEPHANT',
    'F for FOX',
    'G for GOAT',
    'H for HEN',
    'I for INK',
    'J for JUG',
    'K for KITE',
    'L for LION',
    'M for MANGO',
    'N for NEST',
    'O for ORANGE',
    'P for PARROT',
    'Q for QUEEN',
    'R for ROSE',
    'S for SUN',
    'T for TREE',
    'U for UMBRELLA',
    'V for VAN',
    'W for WATCH',
    'X for X-MAS TREE',
    'Y for YAK',
    'Z for ZEBRA',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
            'ALPHABETS',
            style: TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black),
            textAlign: TextAlign.center,
          ),
        ),
        body: Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('${letters[index]}'),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Smile and click me!',
                    style: TextStyle(fontSize: 10),
                  ),
                  TextButton(
                      onPressed: showLetters,
                      child: Icon(
                        Icons.emoji_emotions,
                        color: Colors.yellow,
                      )),
                ],
              )
            ],
          ),
        ));
  }

  void showLetters() {
    setState(() {
      index++;
    });
  }
}

class BizzCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.centerRight,
        color: Colors.white,
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Text(
              'Flutter is a SDK ',
              textAlign: TextAlign.center,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            _getText(),
            _getCard(),
          ],
        ),
      ),
    );
  }

  Container _getText() {
    return Container(
      width: 600,
      height: 300,
      margin: EdgeInsets.all(25.0),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        boxShadow: [
          BoxShadow(color: Colors.black, spreadRadius: 2.8, blurRadius: 5)
        ],
        borderRadius: BorderRadius.circular(32),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'VISMAYA RAJ,',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          Text('Flutter Developer',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.alternate_email,
                color: Colors.black,
              ),
              Text(':'),
              Text('vismayavipz999@gmail.com')
            ],
          )
        ],
      ),
    );
  }

  _getCard() {
    return Container(
      width: 100,
      height: 100,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.elliptical(10.5, 10.5)),
          backgroundBlendMode: BlendMode.color,
          boxShadow: [BoxShadow(color: Colors.black, blurRadius: 10.4)],
          image: DecorationImage(
              image: NetworkImage(
                  'https://flutter.dev/images/catalog-widget-placeholder.png'),
              fit: BoxFit.fill)),
    );
  }
}

class ScaffoldExample extends StatelessWidget {
  void _buttonClick() {
    print('list icon clicked');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('SAMPLE APP',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 29.8,
                  color: Colors.black)),
          backgroundColor: Colors.white,
          centerTitle: true,
          actions: [
            IconButton(
              onPressed: _buttonClick,
              icon: Icon(Icons.list),
              color: Colors.black,
              splashColor: Colors.black,
              tooltip: 'list',
            )
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.dialpad,
                color: Colors.black,
              ),
              label: 'dialbar',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.access_alarm,
                color: Colors.black,
              ),
              label: 'alarm',
            ),
          ],
          onTap: (int index) => print('index position ${index}'),
          iconSize: 24.9,
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          splashColor: Colors.black,
          backgroundColor: Colors.green,
          foregroundColor: Colors.white,
          onPressed: () => print('Hello!'),
        ),
        body: Container(
          alignment: Alignment.center,
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            // CustomButton(),

            InkWell(
              child: Icon(Icons.ac_unit_rounded),
              onDoubleTap: () => print('tapped'),
              splashColor: Colors.purpleAccent.shade700,
            )
          ]),
        ));
  }
}

class CustomButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final snackbar = SnackBar(
          content: Icon(
            Icons.dialpad,
            color: Colors.white,
          ),
          duration: Duration(seconds: 1),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackbar);
      },
      child: Container(
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            color: Colors.amber, borderRadius: BorderRadius.circular(8.0)),
        child: Text('click to see me!'),
      ),
    );
  }
}

class Home extends StatelessWidget {
  Widget build(BuildContext context) {
    return Material(
      color: Colors.deepPurple,
      shadowColor: Colors.black,
      child: Center(
          child: Text(
        'Hello July!',
        textDirection: TextDirection.ltr,
        style: TextStyle(
            fontStyle: FontStyle.italic,
            fontSize: 23.8,
            fontWeight: FontWeight.bold),
      )),
    );
  }
}
